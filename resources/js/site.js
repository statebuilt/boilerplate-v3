/**
 * First we will load all of this project's JavaScript dependencies.
 * We make no assumptions as to what you need or want to use.
 */

require('./bootstrap');


import './gist/drawer-nav';

/**
 * Next, if you want to use Vue.js, you can uncomment the following lines
 * and install it with `npm install vue --save`
 */

window.Vue = require('vue');

import Icons         from './components/Icons/Icons';
import Modal         from './components/Modal';
import VideoPlaylist from './components/VideoPlaylist';

new Vue({
    el: '#site',
	components: {
		Icons,
		Modal,
		VideoPlaylist,
	}
});
