title: About
hero_overlay: hero-overlay
hero_button: true
hero_button_text: 'Kitchen Sink'
hero_button_link: /kitchen-sink/
template: about
id: dfd872a1-4f62-43df-b701-5643ef4aea60
slug: about
blueprint: page
