---
title: Home
hero_overlay: hero-overlay-light
hero_title: 'Awesome features are here'
hero_subtitle: Homepage
hero_copy: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin porttitor eros sed ullamcorper accumsan. Nam nec convallis justo. Duis pharetra eros ac nisi condimentum, non auctor felis placerat. Donec ac blandit tortor, vel pulvinar ipsum.'
hero_button: true
hero_button_text: 'Test Everthing'
hero_button_link: /kitchen-sink/
template: home
slug: home
blueprint: page
updated_by: aa5f1628-ad83-43b7-a1f9-5e434ec34f71
updated_at: 1581962660
hero_heading: 'hero subheading'
hero_subheading: 'Welcome to the boilerplate v3'
hero_intro: 'Each file inside your resources/views directory is a view. Each view can be used in different ways — given different roles and responsibilities.'
hero_image: images/mountains-16x9.jpg
hero_height: hero-height
sections:
  -
    section_meta: null
    section_id: null
    background_color: transparent
    desktop_space: 'dt:frame'
    mobile_space: 'mb:frame'
    full_section: null
    blocks:
      -
        block_meta: null
        space_above: mar-t
        subheading: testing
        heading: 'Main heading'
        type: heading
        enabled: true
      -
        block_meta: null
        space_above: mar-t
        copy: |
          Each file inside your resources/views directory is a view. Each view can be used in different ways — given different roles and responsibilities.
          
          ###Layouts
          
          Layouts are the the foundation of your frontend’s HTML. Any markup you want present no matter what page you’re on, no matter where you go, how far you travel, or loud you sing, should go into a layout.
          
          By default, Statamic will look for and use /resources/views/layout.antlers.html, but you’re welcome to create other layouts and configure specific entries, whole sections, or the whole site to use those instead.
          
          Layouts usually contain <head></head> markup, global header, navigation, footer, JavaScript includes, and so on. In between all that HTML is your template area — the magical place where unique, non-global things happen. Use the {{ template_content }} variable to set where you’d like that live.
        type: copy
        enabled: true
    type: full_row
    enabled: true
  -
    section_meta: null
    section_id: null
    background_color: transparent
    desktop_space: 'dt:frame'
    mobile_space: 'mb:frame'
    left_column_settings: null
    right_column_settings: null
    column_left:
      -
        block_meta: null
        space_above: mar-t
        image: images/mountains-16x9.jpg
        type: image
        enabled: true
    column_right:
      -
        block_meta: null
        space_above: mar-t
        subheading: subheading
        heading: 'First Section'
        type: heading
        enabled: true
      -
        block_meta: null
        space_above: mar-t
        copy: 'By default, Statamic will look for and use /resources/views/layout.antlers.html, but you’re welcome to create other layouts and configure specific entries, whole sections, or the whole site to use those instead.'
        type: copy
        enabled: true
      -
        block_meta: null
        space_above: mar-t
        button_alignment: text-right
        button_text: 'See More'
        button_link: /about/
        type: button
        enabled: true
    type: half_row
    enabled: true
id: 4c325be1-4b8c-42ef-91f9-15f12f7938c3
---
