---
title: 'All the styles'
featured_image: /assets/images/blog-background-001.jpg
hero_overlay: hero-overlay-dark
post_intro: 'Suspendisse dui libero, finibus eget nunc ac, congue lacinia massa. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.'
author: aa5f1628-ad83-43b7-a1f9-5e434ec34f71
bard:
  -
    type: text
    text: |
      <p>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent nec 
      neque a tellus ultricies bibendum vel vitae sapien. Maecenas fermentum 
      ligula nec metus dictum, sed commodo mi mattis. Pellentesque habitant 
      morbi tristique senectus et netus et malesuada fames ac turpis egestas. 
      <a href="wherever" target="_blank" rel="noreferrer">Ut iaculis sem</a> sit amet quam ultricies, non semper magna porta. 
      Vestibulum justo nisl, pharetra placerat mauris ut, dignissim elementum 
      tortor. Aenean sed orci nibh. Vestibulum tincidunt nisl a vestibulum 
      vestibulum. Curabitur leo quam, cursus eu neque vel, auctor semper 
      justo.<br></p>
      
  -
    type: fancy_blockquote
    quote_copy: 'Nunc eget dignissim massa. Nunc lacinia scelerisque tristique. Phasellus iaculis at enim nec sodales.'
  -
    type: text
    text: |
      <p>
      Cras ac feugiat tellus. Suspendisse eu risus viverra dui tincidunt 
      interdum. Nullam eu nisl interdum, finibus neque et, elementum lorem. 
      Nunc ut elementum felis. Nam eu nisi vitae odio varius lacinia vitae 
      fermentum mauris. Donec sollicitudin laoreet lectus, eu sodales nibh 
      rhoncus sed. Nunc mollis sit amet turpis eu sollicitudin. Duis sit amet 
      justo libero. Donec at hendrerit libero, vel interdum odio</p>
      
  -
    type: video
    video: 'https://www.youtube.com/watch?v=NwTsZHGQ6FE'
  -
    type: text
    text: |
      <p>Cras ac feugiat tellus. Suspendisse eu risus viverra dui tincidunt 
      interdum. Nullam eu nisl interdum, finibus neque et, elementum lorem. 
      Nunc ut elementum felis. Nam eu nisi vitae odio varius lacinia vitae 
      fermentum mauris. Donec sollicitudin laoreet lectus, eu sodales nibh 
      rhoncus sed. Nunc mollis sit amet turpis eu sollicitudin. Duis sit amet 
      justo libero. Donec at hendrerit libero, vel interdum odio</p><h2>Some text for header 02</h2><p>Ut venenatis purus bibendum, convallis mauris 
      vitae, molestie ipsum. Cras ac posuere dui, id ultrices nisi. Nunc 
      mattis lorem orci, in viverra metus accumsan sit amet.</p><h3>Some text for header 03</h3><p>Nulla vitae diam 
      id lacus mollis vestibulum. Duis elementum gravida placerat. Nulla 
      posuere gravida nisl et auctor. Aliquam erat volutpat. </p><h4>Some text for header 04</h4><p>Ut venenatis purus bibendum, convallis mauris 
      vitae, molestie ipsum. Cras ac posuere dui, id ultrices nisi. Nunc 
      mattis lorem orci, in viverra metus accumsan sit amet.</p><h5>Some text for header 05</h5><p> Suspendisse dui libero, finibus eget nunc ac, congue lacinia massa. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed ac nunc rutrum, dictum dolor in, eleifend augue. <br></p>
      
  -
    type: image
    alignment: align-left
    image: /assets/images/mountains-16x9.jpg
  -
    type: text
    text: |
      <p>
      Suspendisse eget augue mollis, aliquet augue ut, interdum tellus. 
      Integer erat elit, auctor non ipsum nec, gravida varius nibh. Donec 
      eleifend urna egestas varius vehicula. Donec aliquet aliquet enim non 
      facilisis. Fusce vestibulum lectus sit amet libero porta sollicitudin. 
      Donec accumsan ante sed ipsum viverra, eu maximus mi bibendum. Mauris 
      pellentesque tempor dolor, eget placerat neque hendrerit sit amet. 
      Quisque tincidunt, lectus nec maximus fringilla, turpis mauris rhoncus 
      urna, id feugiat lectus lacus suscipit risus. <br></p>
      
  -
    type: unordered_list
    alignment: align-right
    list:
      - 'List item with some text 01 and some more text'
      - 'List item with some text 02'
      - 'List item with some text 03'
      - 'List item with some text 04'
      - 'List item with some text 05'
  -
    type: text
    text: |
      <p>Duis porta feugiat justo 
      viverra tincidunt. Phasellus eleifend sapien sed leo gravida accumsan. 
      Sed consequat ligula a dui gravida interdum. Nulla eget diam sed tortor 
      sodales gravida. Fusce diam urna, tempus nec ex vestibulum, cursus 
      semper risus. Nullam suscipit, dolor in tempor bibendum, quam nisi 
      posuere elit, sed convallis velit urna in est. Vivamus scelerisque magna
       ut dictum vulputate.&nbsp;</p>
      
  -
    type: ordered_list
    alignment: align-right
    list:
      - 'List item with some text 01 and some more'
      - 'List item with some text 02 and some more'
      - 'List item with some text 03 and some more'
      - 'List item with some text 04 and some more'
      - 'List item with some text 05 and some more'
  -
    type: text
    text: |
      <p>Duis porta feugiat justo 
      viverra tincidunt. Phasellus eleifend sapien sed leo gravida accumsan. 
      Sed consequat ligula a dui gravida interdum. Nulla eget diam sed tortor 
      sodales gravida. Fusce diam urna, tempus nec ex vestibulum, cursus 
      semper risus. Nullam suscipit, dolor in tempor bibendum, quam nisi 
      posuere elit, sed convallis velit urna in est. Vivamus scelerisque magna
       ut dictum vulputate. </p>
      
  -
    type: image
    alignment: align-wide
    image: /assets/images/mountains-16x9.jpg
    caption: 'Mountians in Norway'
  -
    type: text
    text: |
      <p>
      Praesent commodo molestie porta. Vestibulum tempus egestas ipsum, in 
      congue nisi faucibus a. Orci varius natoque penatibus et magnis dis 
      parturient montes, nascetur ridiculus mus. Mauris gravida ultrices 
      massa, et luctus ipsum auctor ut. Mauris non ullamcorper urna. Nunc sit 
      amet euismod purus, a molestie dui. Aenean leo erat, mattis vel erat in,
       feugiat ultrices urna. Cras rhoncus lorem nec convallis porttitor. Duis
       scelerisque aliquam lacus, quis ultrices odio placerat non. Interdum et
       malesuada fames ac ante ipsum primis in faucibus. Pellentesque vel 
      neque justo. <br></p>
      
  -
    type: double_image
    alignment: align-full
    images:
      - /assets/images/mountains-16x9.jpg
      - /assets/images/mountains-9x16.jpg
    caption: 'Lovely Mountians'
  -
    type: text
    text: |
      <p>Praesent commodo molestie porta. Vestibulum tempus egestas ipsum, in 
      congue nisi faucibus a. Orci varius natoque penatibus et magnis dis 
      parturient montes, nascetur ridiculus mus. Mauris gravida ultrices 
      massa, et luctus ipsum auctor ut. Mauris non ullamcorper urna. Nunc sit 
      amet euismod purus, a molestie dui. Aenean leo erat, mattis vel erat in,
       feugiat ultrices urna. Cras rhoncus lorem nec convallis porttitor. Duis
       scelerisque aliquam lacus, quis ultrices odio placerat non. Interdum et
       malesuada fames ac ante ipsum primis in faucibus. Pellentesque vel 
      neque justo.
      </p>
      
template: entry/post
id: 4b9270b0-d194-4059-89fb-3326c0267638
blueprint: post
---