title: 'Kitchen Sink'
hero_overlay: hero-overlay
hero_copy: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam efficitur eu quam et rutrum.'
hero_button: true
hero_button_text: 'Read More'
hero_button_link: '#section_1'
sections:
  -
    type: split_section
    desktop_space: desktop_frame_not
    mobile_space: mobile_frame
    column_on_top: column_left_on_top
    left_background_color: transparent
    right_background_color: transparent
    column_left:
      -
        type: twitter_feed
        space_above: mar-t
    column_right:
      -
        type: image_full
        space_above: mar-t
        image: /assets/images/mountains-16x9.jpg
hide_in_nav: true
template: dev/kitchen-sink
fieldset: page
id: faec12b0-6a0e-4ff9-976c-c38de982b6aa
