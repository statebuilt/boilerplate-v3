title: Home
hero_image: /assets/images/mountains-16x9.jpg
hero_overlay: hero-overlay-light
hero_title: 'Awesome features are here'
hero_subtitle: Homepage
hero_copy: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin porttitor eros sed ullamcorper accumsan. Nam nec convallis justo. Duis pharetra eros ac nisi condimentum, non auctor felis placerat. Donec ac blandit tortor, vel pulvinar ipsum.'
hero_button: true
hero_button_text: 'Test Everthing'
hero_button_link: /kitchen-sink/
sections:
  -
    type: offset_section
    desktop_space: desktop_frame--top
    mobile_space: mobile_frame--top
    background_color: transparent
    main_column:
      -
        type: content_area
        space_above: mar-t
        content_area: |
          <h3>Some Content</h3>
          <p>Suspendisse dui libero, finibus eget nunc ac, congue lacinia massa. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas
          </p>
    aside_column:
      -
        type: social_icons
        space_above: mar-t
      -
        type: filler
  -
    type: half_section
    background_color: transparent
    desktop_space: desktop_frame--top
    mobile_space: mobile_frame--top
    column_on_top: column_right_on_top
    column_left:
      -
        type: heading_content
        space_above: mar-t
        background_color: transparent
        block_subheading: 'subtitle yo'
        block_heading: 'Main title Doohicky'
        block_content: 'Suspendisse dui libero, finibus eget nunc ac, congue lacinia massa. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.'
      -
        type: content_area
        space_above: mar-t
        content_area: |
          <h3>Some content</h3>
          <p>Suspendisse dui libero, finibus eget nunc ac, congue lacinia massa. 
          Pellentesque habitant morbi tristique senectus et netus et malesuada 
          fames ac turpis egestas.
          <br>
          </p>
      -
        type: newsletter_signup
        space_above: mar-t
    column_right:
      -
        type: image_full
        space_above: mar-t
        background_color: transparent
        image: /assets/images/mountains-16x9.jpg
      -
        type: filler
    left_background_color: transparent
    right_background_color: transparent
  -
    type: half_section
    background_color: transparent
    desktop_space: desktop_frame--bot
    mobile_space: mobile_frame--bot
    column_on_top: column_left_on_top
    column_left:
      -
        type: image_full
        space_above: mar-t
        background_color: transparent
        image: /assets/images/mountains-16x9.jpg
    column_right:
      -
        type: heading_content
        space_above: mar-t
        background_color: transparent
        block_subheading: 'subtitle yo'
        block_heading: 'Main title Doohicky'
        block_content: 'Suspendisse dui libero, finibus eget nunc ac, congue lacinia massa. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.'
    left_background_color: transparent
    right_background_color: transparent
template: home
fieldset: homepage
id: 4c325be1-4b8c-42ef-91f9-15f12f7938c3
