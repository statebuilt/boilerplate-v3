let mix = require('laravel-mix');

mix.js('./js/src/boilerplate.js', './js/')
    .sass('./sass/boilerplate.scss', './css/')
    .sourceMaps()
    .webpackConfig({
        devtool: 'source-map'
    })
    .options({
        processCssUrls: false
    });
