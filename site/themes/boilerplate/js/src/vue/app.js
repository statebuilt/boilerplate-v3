
import Vue from 'vue';
import TweenMax from 'gsap';
import Icons from './Components/Icons/Icons';
import Modal from './Components/Modal';
import VideoPlaylist from './Components/VideoPlaylist';

new Vue({
	el: '#app',
	components: {
		Icons,
		Modal,
		VideoPlaylist,
	}
});